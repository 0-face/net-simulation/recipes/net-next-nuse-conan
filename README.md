# net-next-nuse - conan recipe

[Conan.io](https://www.conan.io/) recipe to build [net-next-nuse][homepage] for [DCE][dce_home].


[homepage]: https://direct-code-execution.github.io/net-next-sim/
[dce_home]: https://www.nsnam.org/overview/projects/direct-code-execution/

## About net-next-nuse

The project is a library operating system (LibOS) version of Linux kernel.

See the [project repository][project_repo] for more info.


[project_repo]: https://github.com/libos-nuse/net-next-nuse

## How to use this recipe

This recipe is built to be used with DCE (to enable the linux stack).

See how to build DCE [here][dce_build] and [conan docs][conan_docs]
for instructions in how to use conan.

Also, you can look for our [ns3-dce conan recipe][ns3_dce_recipe]
that should already use this recipe.

[dce_build]: https://ns-3-dce.readthedocs.io/en/latest/getting-started.html#building-dce-using-waf
[conan_docs]: http://docs.conan.io/en/latest/
[ns3_dce_recipe]: https://gitlab.com/0-face/conan-recipes/ns3-dce-conan

## License

This conan package is distributed under the [unlicense](http://unlicense.org/) terms
(see UNLICENSE.md).

*net-next-nuse* however is distributed under the GPLv2 license (see their [repository][project_repo]).

