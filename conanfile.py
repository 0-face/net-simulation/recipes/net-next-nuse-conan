from conans import ConanFile, tools

import os
from os import path

import shutil
import re

class Recipe(ConanFile):
    name           = "net-next-nuse"
    description    = "A library operating system version of Linux kernel"
    license        = "GPLv2"

    version        = "4.4"
    settings       = "os", "compiler", "arch"

    homepage       = "https://direct-code-execution.github.io/net-next-sim/"
    repo           = "https://github.com/libos-nuse/net-next-nuse"
    source_sha256  = "562f9da00c0a7544a201e14f0653d463ca5a7bf83fed43ee15350f7f8546882c"

    def configure(self):
        # package is pure c
        del self.settings.compiler.libcxx

    def source(self):
        self.download_source()

    def build(self):
        with tools.chdir(self.build_dir):
            self.run_and_log("make defconfig ARCH=lib")
            self.run_and_log("make library ARCH=lib")

    def package(self):
        # copy includes
        self.copy("*",
                  src=path.join(self.build_dir, 'arch', 'lib', 'include'),
                  dst=path.join('arch', 'lib', 'include')
        )

        self.package_libs()

    def package_info(self):
        self.cpp_info.includedirs = [path.join('arch','lib', 'include'), path.join('arch','lib')]

################################# sub steps ########################################################

    def download_source(self):
        zip_name = self.name + ".tar.gz"
        url = "{}/archive/libos-v{}.tar.gz".format(self.repo, self.version)

        zip_path = path.join(self.source_folder, zip_name)

        if path.exists(zip_path) and tools.sha256sum(zip_path) == self.source_sha256:
            self.output.info("Skipped source download ('%s' found)" % zip_name)
        else:
            self.output.info("Downloading source code (this may take a while) ...")
            tools.download(url, zip_name)
            tools.check_sha256(zip_name, self.source_sha256)

        self.output.info("Extracting zip...")
        tools.unzip(zip_name)

    def package_libs(self):
        built_libs_dir = path.join(self.build_dir, 'arch', 'lib', 'tools')

        # Copy built library 'libsim-linux' (and symlinks)
        libs = self.find_libs(['sim-linux'], src_dir=built_libs_dir)
        for l in libs:
            self.copy(l, src=built_libs_dir, dst="lib", symlinks=True)

        # Creating links (or copies) to 'libsim-linux' to match name used in DCE (liblinux)
        for l in libs:
            new_name = l.replace('sim-linux', 'linux')
            link_path = path.join(self.package_folder, 'lib', new_name)

            self.output.info("Linking {} to {}".format(link_path, l))
            self.symlink_or_copy(src=l, dst=link_path)

################################# Helpers ########################################################

    def find_libs(self, lib_names, src_dir, versioned=None):
        versions_pattern = r'(\-\d+(\.\d+)*)' # Examples: ['-1.2.3', '-1', '-10.0']

        if versioned is None: # both versioned and not
            versions_pattern += '?' #turn pattern optional
        elif versioned is False:
            versions_pattern = ''

        ext_pattern = r'[\w]+'

        lib_pattern_str = r'(lib)?%s%s\.%s' % ('{}', versions_pattern, ext_pattern)

        all_libs = []

        for l_name in lib_names:
            lib_pattern = re.compile(lib_pattern_str.format(l_name))
            libs = [l for l in os.listdir(src_dir) if lib_pattern.match(l)]

            all_libs.extend(libs)

        return all_libs

    def symlink_or_copy(self, src, dst, *k, **kwargs):
        try:
            os.symlink(src, dst, *k, **kwargs)
        except Exception as err:
            self.output.warn(str(err))

            shutil.copyfile(src, dst, *k, **kwargs)

    def run_and_log(self, cmd, cwd=None, env=None):
        msg = ''
        if cwd:
            msg = "cwd='{}'\n".format(cwd)
        if env:
            msg += "env='{}'\n".format(env)

        if cwd or env:
            msg += "\t"

        self.output.info(msg + cmd)

        self.run(cmd, cwd = cwd)

######################################## Properties ####################################################

    @property
    def build_dir(self):
        return path.join('net-next-nuse-libos-v{}'.format(self.version))
