# To speed up 'apt-get update', we will update only
# from 'main' repository

# We get distribution version
cat /etc/lsb-release
. /etc/lsb-release

# Then, create a file with the repository to update from
echo "deb http://archive.ubuntu.com/ubuntu/ $DISTRIB_CODENAME main restricted" > source.list

# Update only that repository
sudo apt-get update -o Dir::Etc::sourcelist="./source.list" \
    -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"

# Install dependencies
sudo apt-get install -y bc
